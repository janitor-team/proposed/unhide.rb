PREFIX=/usr

all :
	@echo Nothing to do for all, try \"make install\".

install :
	install unhide.rb ${PREFIX}/bin
	install --mode=644 unhide.rb.8 ${PREFIX}/share/man/man8
